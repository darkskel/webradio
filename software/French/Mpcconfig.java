/**
	*	Mpcconfig.java 		04/09/2014
*/
import java.io.*;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import java.text.DateFormat;

/**
	* Programme d'automation webradio
	* @author Remy gaffard
	* @version 1.0
*/
public class Mpcconfig {
	
	/** Nom de la playlist aleatoire*/
	public static String aleatoire="Aleatoire";
	
	/** Mode silense */
	public static boolean silence = false;	
	
	private static String host = "";
	
	private static String port = "";
	
	/**
		* Permet de recuper les parametres et les stocks dans des variables 
	*/
	public static void config(){
		String resultat, silenceRecup;
		int a =0;
		try{
			Scanner scanner = new Scanner(new File("parametre.txt"));
			while(scanner.hasNextLine()){				
				if(a == 0){
					resultat = scanner.nextLine();
					aleatoire = resultat.replace("Nom PlayList aleatoire = ", "");					
					}else if(a == 1){
					resultat = scanner.nextLine();
					silenceRecup = resultat.replace("Mode Silence = ", "");	
					if(silenceRecup.equals("on")){
						silence = true;
					}					
					}else if(a == 2){
					resultat = scanner.nextLine();
					host = resultat.replace("Host = ", "");											
					}else if(a == 3){
					resultat = scanner.nextLine();
					port = resultat.replace("Port = ", "");											
				}
				a++;
			}					
			scanner.close();
			
		}
		catch(Exception e){
			System.out.println("Erreur avec la récupération des parametres");
			System.out.println("Erreurs possible : ");			
			System.out.println("Pas de fichier trouver verifier que parametre.txt existe");		
			System.out.println("Format incorrect la premier ligne doit etre de la forme Nom PlayList aleatoire = Nom");
			System.out.println("Format incorrect la 2eme ligne doit etre de la forme Nom Mode Silence = on/off");
		}
		
	} 
	
	/** recupere l'heure minute seconde
		* @return int heure minute
	*/
	public static double heure(){
		
		double heure=0.0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm");
		Date now = new Date();
		heure = Double.parseDouble( dateFormat.format(now));
		return heure;
		
	}
	
	/** recupere le jour
		* @return String jour
	*/
    public static String jour(){
		
        String jour="lundi";
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEEEEE", Locale.FRENCH );
        Date now = new Date();
        jour = dateFormat.format(now);
		return jour;
		
	}
	
	
	/** fonction de lecture de la playliste a jouer 
		* @param String le nom fichier
		* @return la playlist a jouer
	*/
	private static String lecturePlaylist( String fichier){
		
		String resultat= "next";
		double heure= 0.0;
		String nom= "";
		try{
			Scanner scanner = new Scanner(new File("planning/" + fichier + ".txt"));
			while(scanner.hasNextLine()){
				String[] recupResultat = scanner.nextLine().split(";");				
				heure = Double.parseDouble(recupResultat[0]);
				nom = recupResultat[1];
				// Si l'heure trouver == l'heure local
				if(heure == heure()){
					resultat = nom;
					break;
				}
			}					
			scanner.close();
			
		}
		catch(Exception e){
			System.out.println("Erreur avec la récupération des playlists");
			System.out.println("Erreurs possible : ");
			System.out.println("Pas de fichier trouver verifier que " + fichier + ".txt existe");
			System.out.println("Format incorrect chaque lignes doit étre de la forme 00.00;Nom alea (alea est optionnel)");
			resultat= "next";
		}
		return resultat;
	}
	/** fonction de lecture de la dernier musique de la playsliste
		* @return la playlist a jouer
	*/
	private static String finPlaylist(){
		
		String 	resultat="";	
		try{
			Process proc=Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" playlist");
			InputStream in = proc.getInputStream();
			BufferedWriter out=new BufferedWriter(new FileWriter("finplaylist.txt"));
			
			int c;
			while ((c = in.read()) != -1) {
				out.write((char)c);
			}
			
			in.close();
			out.flush();
			out.close();
			
			
			Scanner scanner = new Scanner(new File("finplaylist.txt"));
			
			while (scanner.hasNextLine()) {
				resultat = scanner.nextLine();
			}
			scanner.close();
		}
		catch(Exception e){
			System.out.println("Pas de playlist trouver");
		}
		return resultat;
	}
	/** fonction pour recuperer le nom de la musique jouer 
		@return la musique jouer en se moment
	*/
	private static String lectureEnCour(){
		
		String 	resultat="";
		try{
            Process proc=Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" current");
            InputStream in = proc.getInputStream();
			BufferedWriter out=new BufferedWriter(new FileWriter("son.txt"));
			
			int c;
            while ((c = in.read()) != -1) {
                out.write((char)c);
			}
			
			in.close();
			out.flush();
			out.close();
		}
		catch(Exception e){
			System.out.println("Pas de musique trouver");
		}
		
		try{
			Scanner scanner = new Scanner(new File("son.txt"));
			resultat = scanner.nextLine();			
			scanner.close();
			
		}
		catch(Exception e){
			System.out.println("Fichier son.txt non trouver");
		}		
		return resultat;
	}
	
	
	/** fonction de programmation de la webradio 
		@param argument non utiliser	
	*/
	public static void main(String[] args){
		
		System.out.println(" "); // pour afficher proprement au boot 
		System.out.println("Lancement de la programation le " + jour() + " a " + heure() + "h!" );
		config();
		try {
			Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" play"); //lance la playlist
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		TimerTask task = new TimerTask()
		{
			public void run()
			{	
				// La mention special next pour pas couper le programme en cour de diffusion 
 				if(lecturePlaylist(jour()).equals("next")){				
 				}
 				else {
					try {						
						String chargement= lecturePlaylist(jour());
						String tampon = chargement;
						// si random activer 
						Pattern p = Pattern.compile("\\w+\\s+\\w+");
						Matcher a = p.matcher(chargement);	
						// si sa respecte le schema "00:00;playliste alea"						
						if(a.matches()){
							
							String[] ecoute= Pattern.compile("\\s").split(chargement);					
							if(ecoute[1].equals("alea")){
								Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" random on");								
								}else{
								Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" random off");
							}
							tampon = ecoute[0];
							if(!tampon.isEmpty()){
								Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" crop");
								Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" update");
								Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" load " + tampon);					
							}
							}else{							
							Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" random off");
						}
						if(!silence){							
							System.out.println(tampon);
						}					
					
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				if(!silence){
					System.out.println("Mise en place Succes");
				}			
			}
			// Comblage 
			if(lectureEnCour().equals(finPlaylist())){
				if(!silence){
					System.out.println("comblage!");
				}
				try {
					Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" random on");     
					Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" crop");		     
					Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" update");
					Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" load " + aleatoire );
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				if(!silence){
					System.out.println("Mise en place Succes");
				}
			}
			try {
				Runtime.getRuntime().exec("mpc -h "+host+"  -p "+port+" play"); //lance la playlist
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(task, 0,60000);
	}
}