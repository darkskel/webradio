Avant d'utilise ce logiciel vous devez avoir sur votre raspberry
- Java 8+
- mpd
- mpc
- une/des playlist(s) sous format m3u (pouvant etre generer via l'interface web ou depuis un client mpd exemple : Gnome Music Player Client) 

Pour installer le logiciel il suffit de placer les fichiers dans le dossier que vous voulez et de lancer le .class en ligne de commande
"java Mpcconfig"

Mpd dois etre lanc� sinon un message d'erreur s'affichera 

Pour configurer l'applie il faut editer le fichier parametre.txt

Nom PlayList aleatoire = Aleatoire  << definit la playlist qui sera utilis� par defaut 
Mode Silence = off << Definit si les messages non important son ignor� ou pas

Pour ajouter une playliste, il faut modifier le fichier du jour de la semaine dans planning

Chaque ligne correspond � une programmation

Pour ajout� une programation il suffit de respecter la forme suivante

heure;nomDeLaPlaylist [alea]

exemple

18.30;MaSuperPlaylist 
9.54;Remix alea

L'option alea permet de mettre la playlist en lecture aleatoire plutot que les un � la suite

Toute ces manipulations sont fait de maniere transparente par l'interface web mais demande un server web et icecast/edcast
